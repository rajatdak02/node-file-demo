const user = require('./user');
const restro = require('./restaurent');

const initiateRoutes = (router) => {
  user(router);
  restro(router);
};

module.exports = initiateRoutes;

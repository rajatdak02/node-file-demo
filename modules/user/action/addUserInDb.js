const { users } = require(__basedir + '/db/controllers');

const addUserInDb = async (userObj) => {
  const result = await users.getUser({ email: userObj.email });

  if (result) {
    throw new Error('User already exist in db');
  }

  await users.createUser(userObj);
  return 'User added successfully in db';
};

module.exports = {
  addUserInDb
};

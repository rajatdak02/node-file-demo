const { addUserDataInFile } = require('./addUserDataInFile');
const { loginViaFile } = require('./loginViaFile');
const { addUserInDb } = require('./addUserInDb');

module.exports = {
  addUserDataInFile,
  loginViaFile,
  addUserInDb
};

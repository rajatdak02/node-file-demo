const { join } = require('path');
const bcrypt = require('bcrypt');
const {
  readFilePromisify,
  writeFilePromisify
} = require('../../../helper/promisify');

const completePath = join('public', 'user.txt');

const addUserDataInFile = async (userData) => {
  try {
    let fileData = await readFilePromisify(completePath, 'utf-8');
    const { email, password } = userData;
    let parseData = [];
    if (fileData) {
      parseData = JSON.parse(fileData);
    }

    const findData = parseData.find((p) => p.email === email);

    if (findData) {
      throw new Error('User already exist');
    } else {
      const hashedPassword = await bcrypt.hash(password, 10);
      const updatedData = { email, password: hashedPassword };
      parseData.push(updatedData);
      await writeFilePromisify(completePath, JSON.stringify(parseData));
      return 'User added successfully';
    }
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = {
  addUserDataInFile
};

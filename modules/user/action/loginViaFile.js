const { join } = require('path');
const bcrypt = require('bcrypt');
const { createToken } = require(__basedir + '/middleware');
const { readFilePromisify } = require('../../../helper/promisify');

const completePath = join('public', 'user.txt');

const loginViaFile = async (userData) => {
  try {
    let fileData = await readFilePromisify(completePath, 'utf-8');
    const { email, password } = userData;
    let parseData = [];
    if (fileData) {
      parseData = JSON.parse(fileData);
    }
    const foundUser = parseData.find((p) => p.email === email);

    if (foundUser) {
      const match = await bcrypt.compare(password, foundUser.password);
      if (match) {
        const token = await createToken({ email });
        return { data: 'user login successfully', token };
      } else {
        throw new Error('Invalid password');
      }
    } else {
      throw new Error('Invalid email or password');
    }
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = {
  loginViaFile
};

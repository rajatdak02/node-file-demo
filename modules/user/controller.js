const { addUserDataInFile, loginViaFile, addUserInDb } = require('./action');

const addUserInFile = async (req, res) => {
  try {
    const userData = req.body;
    // const data = await addUserDataInFile(userData);

    const data = await addUserInDb(userData);
    res.status(200).send({ data });
  } catch (error) {
    console.log('error', error);
    res.status(500).send({ error: 'Something went wrong' });
  }
};

const login = async (req, res) => {
  try {
    const userInfo = req.body;
    const data = await loginViaFile(userInfo);
    res.status(200).send({ data });
  } catch (error) {
    console.log('error', error);
    res.status(500).send({ error: 'Something went wrong' });
  }
};

module.exports = {
  addUserInFile,
  login
};

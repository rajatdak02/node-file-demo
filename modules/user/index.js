const { addUserInFile, login } = require('./controller');
const { isTokenAuthenticated } = require(__basedir + '/middleware');

module.exports = (router) => {
  router.post('/login', login);
  router.post('/add-user', addUserInFile);
};

const { addRestroInFile } = require('./action');

const addRestro = async (req, res) => {
  try {
    const restroData = req.body;
    const data = await addRestroInFile(restroData);
    res.status(200).send({ data });
  } catch (error) {
    console.log('error', error);
    res.status(500).send({ error: 'Something went wrong' });
  }
};

module.exports = {
  addRestro
};

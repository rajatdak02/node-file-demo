const { addRestro } = require('./controller');
const { isTokenAuthenticated } = require('../../middleware');

module.exports = (router) => {
  router.post('/add-restro', isTokenAuthenticated, addRestro);
};

const { join } = require('path');
const {
  readFilePromisify,
  writeFilePromisify
} = require('../../../helper/promisify');

const completePath = join('public', 'restro.txt');

const addRestroInFile = async (restroData) => {
  try {
    let fileData = await readFilePromisify(completePath, 'utf-8');
    let parseData = [];
    if (fileData) {
      parseData = JSON.parse(fileData);
    }

    const findData = parseData.find((p) => p.name === restroData.name);
    if (findData) {
      throw new Error('Restaurent already exist');
    } else {
      parseData.push(restroData);

      await writeFilePromisify(completePath, JSON.stringify(parseData));
      return 'Restaurent added successfully';
    }
  } catch (err) {
    throw new Error(err);
  }
};

module.exports = {
  addRestroInFile
};

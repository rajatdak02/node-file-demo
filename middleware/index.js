const { isTokenAuthenticated, createToken } = require('./auth');

module.exports = {
  createToken,
  isTokenAuthenticated
};

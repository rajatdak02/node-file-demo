const { sign, verify } = require('jsonwebtoken');
const { constants } = require(__basedir + '/config');
const { SECRET } = constants;

const createToken = (payload) => {
  const tokenPayload = Object.assign({ time: new Date().getTime() }, payload);
  return sign(tokenPayload, SECRET, { expiresIn: '1 days' });
};

const isTokenAuthenticated = async (req, res, next) => {
  try {
    const auth = req.headers.authorization;
    if (!auth) {
      throw new Error('Access Denied');
    }
    const authParts = auth.split(' ');
    if (authParts.length !== 2) {
      throw new Error('Format is: Bearer <token>');
    }
    const [scheme, token] = authParts;
    if (new RegExp('^Bearer$').test(scheme)) {
      try {
        await verify(token, SECRET);
        next();
      } catch (e) {
        throw new Error(e);
      }
    } else {
      throw new Error('Format is: Bearer <token>');
    }
  } catch (error) {
    return res.status(401).send({ error: 'User is not authenticate' });
  }
};

module.exports = {
  createToken,
  isTokenAuthenticated
};

const { config } = require('dotenv');
const dotenv = config();

if (dotenv.error) {
  throw dotenv.error;
}

const { PORT, SECRET, SALT_ROUND, MONGO_URI } = process.env;

const constants = {
  PORT,
  SECRET,
  SALT_ROUND,
  MONGO_URI,
  ERROR: {
    BAD_REQUEST: {
      TYPE: 'BAD_REQUEST',
      CODE: 400
    },
    NOT_FOUND: {
      TYPE: 'NOT_FOUND',
      CODE: 404
    },
    INTERNAL_SERVER_ERROR: {
      TYPE: 'INTERNAL_SERVER_ERROR',
      CODE: 500
    },
    UNAUTHORIZED: {
      TYPE: 'UNAUTHORIZED',
      CODE: 403
    },
    UNAUTHENTICATED: {
      TYPE: 'UNAUTHENTICATED',
      CODE: 401
    }
  }
};

module.exports = {
  constants
};

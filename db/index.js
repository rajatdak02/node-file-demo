const { connectToDB } = require('./connection');

module.exports = {
  connectToDB
};

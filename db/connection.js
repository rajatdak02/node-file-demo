const mongoose = require('mongoose');

const { constants } = require(__basedir + '/config');
const { MONGO_URI } = constants;

const connectToDB = () => {
  mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });

  mongoose.connection.on('connected', () => {
    console.log('connected');
  });

  mongoose.connection.on('error', () => {
    console.log('error');
  });

  mongoose.connection.on('disconnected', () => {
    console.log('disconnected');
  });
};

module.exports = {
  connectToDB
};

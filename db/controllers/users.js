const { User } = require('./../models');

const createUser = (userObj) => {
  const user = new User(userObj);
  return user.save();
};

const getUserById = (userId, selection = {}) =>
  User.findOne(
    {
      _id: userId
    },
    selection
  ).lean();

const getUser = (condition = {}, selection = {}) =>
  User.findOne(condition, selection).lean();

module.exports = {
  createUser,
  getUserById,
  getUser
};

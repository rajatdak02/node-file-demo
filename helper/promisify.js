const { promisify } = require('util');
const fs = require('fs');

const readFilePromisify = promisify(fs.readFile);
const writeFilePromisify = promisify(fs.writeFile);

module.exports = {
  readFilePromisify,
  writeFilePromisify
};

global.__basedir = __dirname;

const { constants } = require('./config');
const http = require('http');
const { PORT } = constants;
const { app, router } = require('./app');
const initialRoutes = require('./modules');
const { connectToDB } = require('./db');

initialRoutes(router);

connectToDB();

// Event listeners to catch uncaught errors
process.on('unhandledRejection', (error) => {
  process.exit(1);
});

const server = http.createServer(app);
server.listen(PORT, (err) => {
  if (err) {
    return console.log(`Something went wrong:`);
  }
  console.log(`server lisning on ${PORT}`);
});
